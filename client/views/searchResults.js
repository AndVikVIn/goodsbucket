import {JetView} from 'webix-jet';

class SearchResults extends JetView {
    config() {
        return {
            view: 'tabview',
            localId: 'searchResults',
            hidden: true,
            cells: [
                this.createView('supplier1'),
                this.createView('supplier2'),
                this.createView('supplier3'),
                this.createView('supplier4'),
                this.createView('supplier5'),
                this.createView('supplier6'),
                this.createView('supplier7'),
                this.createView('supplier8'),
                this.createView('supplier9'),
                this.createView('supplier10')
            ]
        };
    }
    createView(uniqueId) {
        return {
            header: '',
            body: {
                view: 'datatable',
                localId: uniqueId,
                scheme: {
                    $init: (obj) => {
                        const currentType = this.typesName.find(item => item.id === obj.type_id);
                        obj.typeName = currentType.name;
                    }
                },
                editable: true,
                editaction: 'click',
                columns: [
                    {id: 'name', header: 'Product name', fillspace: true},
                    {id: 'typeName', header: 'Product type', fillspace: true},
                    {id: 'quantity', header: 'Available amount'},
                    {id: 'price', header: 'Price'},
                    {id: 'addedGoods', header: 'Desireable amount', editor: 'text', numberFormat: '111'},
                    {id: 'addToBusket', header: 'Add to basket', template: '{common.checkbox()}'}
                ],
                on: {
                    onCheck: this.onCheckFunction
                }
            }
        };
    }
    onCheckFunction(rowId) {
        const selectedItem = this.getItem(rowId);
        if (selectedItem.addToBusket === 0) {
            selectedItem.quantity += selectedItem.addedGoods;
        }
        if (selectedItem.addedGoods > selectedItem.quantity ||
            selectedItem.addedGoods === undefined
            || selectedItem.addedGoods === 0 || selectedItem.addedGoods === '') {
            this.blockEvent();
            selectedItem.addToBusket = 0;
            this.updateItem(rowId, selectedItem);
            this.unblockEvent();
            webix.message('Incorrect amount had been choosen', 'error');
        }
        else {
            webix.ajax().post('api/updateAddedQuantity', selectedItem)
                .then((response) => {
                    const parsedResponse = response.json();
                    if (parsedResponse.code === 404) {
                        this.blockEvent();
                        selectedItem.addToBusket = 0;
                        this.updateItem(rowId, selectedItem);
                        this.unblockEvent();
                        webix.message(`${parsedResponse.response.msg}`, 'error');
                    }
                    else {
                        webix.ajax().post('api/updateProductQuantity', selectedItem)
                            .then((res) => {
                                const parsedProductResponse = res.json();
                                if (parsedProductResponse.code === 404) {
                                    this.blockEvent();
                                    selectedItem.addToBusket = 0;
                                    this.updateItem(rowId, selectedItem);
                                    this.unblockEvent();
                                }
                                else {
                                    if (selectedItem.addToBusket === 1) {
                                        selectedItem.quantity -= selectedItem.addedGoods;
                                    }
                                    this.updateItem(rowId, selectedItem);
                                }
                            });
                    }
                })
                .catch(() => {
                    webix.message('Server error occured', 'error');
                });
        }
    }
    ready(view) {
        this.on(this.app, 'parseSearchResults', (suppliers, types) => {
            this.typesName = types;
            let supplierTableNumber = 1;
            const tabbar = this.$$('searchResults').getTabbar();
            suppliers.forEach((supplier) => {
                tabbar.config.options[supplierTableNumber - 1].value = supplier.name;
                this.$$(`supplier${supplierTableNumber}`).clearAll();
                this.$$(`supplier${supplierTableNumber}`).parse(supplier.products);
                supplierTableNumber++;
            });
            
            tabbar.refresh();
            view.show();
        });
    }
}

export default SearchResults;

