/* eslint-disable class-methods-use-this */
import {JetView} from 'webix-jet';
import SearchResults from './searchResults';

class SearchPage extends JetView {
    config() {
        const typeSelect = {
            view: 'select',
            localId: 'typeSelect',
            label: 'Select type',
            labelWidth: 90,
            options: 'api/getAllTypes'
        };

        const addTypeToSearch = {
            view: 'button',
            value: 'Add type for search',
            width: 200,
            click: () => {
                this.app.callEvent('addSearchType');
            }
        };

        const selectResults = {
            localId: 'selectResults',
            name: 'selectResults',
            template: '<span></span>',
            borderless: true
        };

        const clearSelectResults = {
            view: 'button',
            localId: 'clearButton',
            value: 'Clear',
            width: 70,
            click: () => {
                this.app.callEvent('clearSelectResults');
            }
        };


        const firstSortRule = {
            view: 'select',
            name: 'firstRule',
            label: 'First rule',
            labelPosition: 'top',
            options: [
                {id: 'supplier', value: 'By supplier rating'},
                {id: 'type', value: 'By types'},
                {id: 'price', value: 'By price'}
            ]
        };

        const secondSortRule = {
            view: 'select',
            name: 'secondRule',
            label: 'Second rule',
            labelPosition: 'top',
            options: [
                {id: 'type', value: 'By types'},
                {id: 'supplier', value: 'By supplier rating'},
                {id: 'price', value: 'By price'}
            ]
        };
        const thirdSortRule = {
            view: 'select',
            name: 'thirdRule',
            label: 'Third rule',
            labelPosition: 'top',
            options: [
                {id: 'price', value: 'By price'},
                {id: 'type', value: 'By types'},
                {id: 'supplier', value: 'By supplier rating'}
            ]
        };

        const searchButton = {
            view: 'button',
            value: 'Search',
            width: 150,
            click: () => {
                this.app.callEvent('searchForGoods');
            }
        };

        const sortForm = {
            view: 'form',
            localId: 'sortForm',
            elements: [
                {
                    cols: [
                        typeSelect,
                        addTypeToSearch,
                        selectResults,
                        clearSelectResults
                    ]
                },
                {
                    cols: [
                        firstSortRule,
                        secondSortRule,
                        thirdSortRule
                    ]
                }
            ]
        };

        return {
            rows: [
                sortForm,
                {
                    cols: [
                        {},
                        searchButton
                    ]
                },
                SearchResults
            ]
        };
    }
    init() {
        let currentGoodsSearch = '';
        let currentGoodsId = [];
        this.on(this.app, 'addSearchType', () => {
            const value = this.$$('typeSelect').getValue();
            const allTypes = this.$$('typeSelect').config.options.serialize();
            const addedType = allTypes.find(item => item.id === Number(value));
            currentGoodsId.push(addedType.id);
            if (currentGoodsSearch === '') {
                currentGoodsSearch = addedType.name;
            }
            else {
                currentGoodsSearch = `${currentGoodsSearch} , ${addedType.name}`;
            }
            this.$$('selectResults').define('template', `<span>${currentGoodsSearch}</span>`);
            this.$$('selectResults').refresh();
        });
        this.on(this.app, 'searchForGoods', () => {
            const formValues = this.$$('sortForm').getValues();
            if (
                formValues.firstRule === formValues.secondRule ||
                    formValues.firstRule === formValues.thirdRule
                    || formValues.secondRule === formValues.thirdRule) {
                webix.message('Please select different filter rules', 'error');
            }
            else {
                webix.ajax().get('api/searchGoods', {goods: currentGoodsId, searchParams: formValues})
                    .then((res) => {
                        let searchResults = res.json();
                        searchResults = searchResults.reduce((acc, val) => acc.concat(val), []);
                        searchResults.length = 10;
                        const allTypes = this.$$('typeSelect').config.options.serialize();
                        this.app.callEvent('parseSearchResults', [searchResults, allTypes]);
                    });
            }
        });
        this.on(this.app, 'clearSelectResults', () => {
            currentGoodsId.pop();
            const splitedSeletResult = currentGoodsSearch.split(',');
            splitedSeletResult.pop();
            currentGoodsSearch = splitedSeletResult.join(',');
            this.$$('selectResults').define('template', `<span>${currentGoodsSearch}</span>`);
            this.$$('selectResults').refresh();
        });
    }
}

export default SearchPage;
