import {JetView} from 'webix-jet';
import '../styles/app.css';
import BucketTable from './bucketTable';

class BucketPage extends JetView {
    config() {
        const typeSelect = {
            view: 'select',
            localId: 'typeSelect',
            label: 'Select type',
            labelWidth: 90,
            options: 'api/getAllTypes'
        };

        const textInput = {
            view: 'text',
            localId: 'textInput',
            label: 'Set quantity',
            labelWidth: 100,
            width: 180,
            pattern: {mask: '###', allow: /[0-9]/g}
        };

        const addTypeButton = {
            view: 'button',
            value: 'Add to bucket',
            width: 150,
            click: () => {
                const typeId = this.$$('typeSelect').getValue();
                const allTypes = this.$$('typeSelect').config.options.serialize();
                const addedType = allTypes.find(item => item.id === Number(typeId));
                const quantity = this.$$('textInput').getValue();
                if (quantity > 10) {
                    webix.message('max quantity is 10', 'error');
                    return;
                }
                this.app.callEvent('addTypeToBucket', [addedType, quantity, typeId]);
            }
        };
        return {
            rows: [
                {
                    cols: [
                        typeSelect,
                        textInput,
                        addTypeButton
                    ]
                },
                BucketTable
            ]
        };
    }
}

export default BucketPage;
