import {JetView} from 'webix-jet';

class MainPage extends JetView {
    config() {
        const navList = {
            view: 'list',
            localId: 'navList',
            width: 250,
            select: true,
            scroll: false,
            template: '#value#',
            data: [
                {id: 'bucketPage', value: 'Bucket'},
                {id: 'searchPage', value: 'Search'}
            ],
            on: {
                onAfterSelect: (obj) => {
                    this.app.show(`/mainPage/${obj}`);
                }
            }
        };
        return {
            cols: [
                navList,
                {$subview: true}
            ]
        };
    }
    ready(view, url) {
        if (url[1]) {
            this.$$('navList').select(url[1].page);
        }
        else {
            this.$$('navList').select(this.$$('navList').getFirstId());
        }
    }
}

export default MainPage;
