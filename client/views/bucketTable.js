import {JetView} from 'webix-jet';

class BucketTable extends JetView {
    config() {
        return {
            view: 'datatable',
            localId: 'bucketTable',
            url: 'api/getBucketGoods',
            editable: true,
            editaction: 'dblclick',
            save: 'api/changeBucketGoods',
            scheme: {
                $change: (obj) => {
                    if (obj.addedGoods === 0) {
                        obj.$css = 'notFound';
                    }
                    else if (obj.addedGoods === obj.quantity) {
                        obj.$css = 'completed';
                    }
                    else {
                        obj.$css = 'notCompleted';
                    }
                }
            },
            columns: [
                {id: 'typeName', header: 'Product type', fillspace: true},
                {id: 'searchButton', header: '', template: '<span class="fas fa-search"></span>'},
                {id: 'quantity', header: 'Quantity', editor: 'text', numberFormat: '111'},
                {id: 'addedGoods', header: 'Added goods'},
                {id: 'delCol', header: '', template: '{common.trashIcon()}'}
            ],
            onClick: {
                'wxi-trash': (e, id) => {
                    this.$$('bucketTable').remove(id);
                },
                'fa-search': (e, item) => {
                    const searchItem = this.$$('bucketTable').getItem(item.row);
                    // search logic here
                }
            }
        };
    }
    init(view) {
        this.on(this.app, 'addTypeToBucket', (addedType, quantity, typeId) => {
            const alreadyInBucket = view.serialize();
            const isGoodInBucket = alreadyInBucket.find(item => item.name === addedType.name);
            if (isGoodInBucket) {
                webix.message('This good already in your busket', 'error');
                return;
            }
            const newType = {
                name: addedType.name,
                quantity: Number(quantity),
                typeName: addedType.name,
                typeId,
                addedGoods: 0
            };
            view.add(newType);
        });
    }
}
export default BucketTable;
