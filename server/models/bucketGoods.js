module.exports = (sequelize, DataTypes) => {
    const bucketGoods = sequelize.define('goodsInBucket', {
        productName: {
            type: DataTypes.STRING
        },
        quantity: {
            type: DataTypes.INTEGER
        },
        typeId: {
            type: DataTypes.INTEGER
        },
        addedGoods: {
            type: DataTypes.INTEGER
        }
    });

    const ProductType = sequelize.import('../models/productsType');
    ProductType.hasMany(bucketGoods, {foreignKey: 'typeId'});
    bucketGoods.belongsTo(ProductType, {as: 'typeName', foreignKey: 'typeId'});

    bucketGoods.getAll = async () => {
        const result = await bucketGoods.findAll({});
        for (let i = 0; i < result.length; i++) {
            const type = await result[i].getTypeName();
            result[i].dataValues.typeName = type.name;
        }
        return result;
    };

    bucketGoods.createNew = async (values) => {
        const result = await bucketGoods.create(values);
        return result;
    };

    bucketGoods.updateOne = async (id, values) => {
        if (values.amount) {
            values.quantity = values.amount;
        }
        const result = await bucketGoods.update(values, {where: {id}});
        return result;
    };

    bucketGoods.deleteOne = async (id) => {
        const result = await bucketGoods.destroy({where: {id}});
        return result;
    };

    return bucketGoods;
};
