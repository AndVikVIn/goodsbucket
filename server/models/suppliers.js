module.exports = (sequelize, DataTypes) => {
    const supplier = sequelize.define('supplier', {
        name: {
            type: DataTypes.STRING
        },
        rating: {
            type: DataTypes.INTEGER
        }
    });
    
    return supplier;
};
