module.exports = (sequelize, DataTypes) => {
    const productType = sequelize.define('productsType', {
        name: {
            type: DataTypes.STRING
        }
    });

    productType.getAll = async () => {
        const result = await productType.findAll({});
        result.forEach((item) => {
            item.dataValues.value = item.dataValues.name;
        });
        return result;
    };
    const product = sequelize.import('../models/products');
    productType.hasMany(product, {as: 'Products', foreignKey: 'type_id'});
    product.belongsTo(productType, {foreignKey: 'type_id'});


    return productType;
};
