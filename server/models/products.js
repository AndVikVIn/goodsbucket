const Sequelize = require('sequelize');

const Op = Sequelize.Op;

module.exports = (sequelize, DataTypes) => {
    const product = sequelize.define('product', {
        name: {
            type: DataTypes.STRING
        },
        quantity: {
            type: DataTypes.INTEGER
        },
        price: {
            type: DataTypes.INTEGER
        },
        supplier_id: {
            type: DataTypes.INTEGER
        }
    });

    const supplier = sequelize.import('../models/suppliers');
    supplier.hasMany(product, {as: 'Products', foreignKey: 'supplier_id'});
    product.belongsTo(supplier, {foreignKey: 'supplier_id'});

    product.returnSearchResults = async (typesId, searchParams) => {
        let preSortedSuppliers = [];
        const filterParams = Object.values(JSON.parse(searchParams));
        const parsedTypesId = JSON.parse(typesId);
        let unsortedSuppliers = await supplier.findAll({});
        const suppliers = [];
        const getSupliersWhoHasSearchGoods = async () => {
            for (let i = 0; i < unsortedSuppliers.length; i++) {
                const supplierProducts = await unsortedSuppliers[i].getProducts();
                unsortedSuppliers[i].dataValues.products = supplierProducts;
                let sortedSupplier;
                parsedTypesId.forEach((id) => {
                    const condition = supplierProducts.find(currentItem => currentItem.dataValues.type_id === id);
                    if (condition) {
                        if (!sortedSupplier) {
                            sortedSupplier = unsortedSuppliers[i];
                        }
                    }
                });
                if (sortedSupplier) {
                    suppliers.push(sortedSupplier);
                }
            }
        };

        const getBestSuppliers = async (allSuppliers) => {
            allSuppliers.sort((supplier1, supplier2) => {
                let result = 0;
                if (supplier1.dataValues.rating > supplier2.dataValues.rating) {
                    result = -1;
                }
                else if (supplier1.dataValues.rating < supplier2.dataValues.rating) {
                    result = 1;
                }
                return result;
            });
            const ratingArr = [];
            allSuppliers.forEach((currentSupplier) => {
                const alreadyIn = ratingArr.find(item => item === currentSupplier.dataValues.rating);
                if (!alreadyIn) {
                    ratingArr.push(currentSupplier.dataValues.rating);
                }
            });
            ratingArr.forEach(async (rating) => {
                const currentMatchSuppliers = await allSuppliers.filter(item => item.dataValues.rating === rating);
                preSortedSuppliers.push(currentMatchSuppliers);
            });
        };
        const getMoreMatches = async (allSuppliers) => {
            const matchesArr = [];
            for (let i = 0; i < allSuppliers.length; i++) {
                let matches = 0;
                const supplierProducts = await allSuppliers[i].getProducts();
                allSuppliers[i].dataValues.products = supplierProducts;
                parsedTypesId.forEach((id) => {
                    supplierProducts.forEach((currentItem) => {
                        if (currentItem.dataValues.type_id === id) {
                            matches += 1;
                        }
                    });
                    const alreadyIn = matchesArr.find(item => matches === item);
                    if (!alreadyIn) {
                        matchesArr.push(matches);
                    }
                    allSuppliers[i].dataValues.matches = matches;
                });
            }
            matchesArr.sort((a, b) => b - a);
            matchesArr.forEach(async (match) => {
                const currentMatchSuppliers = await allSuppliers.filter(currentSupplier => currentSupplier.dataValues.matches === match);
                if (currentMatchSuppliers[0]) {
                    preSortedSuppliers.push(currentMatchSuppliers);
                }
            });
        };

        const getLowerPrices = async (suppliersArr) => {
            const samePrice = [];
            for (let i = 0; i < suppliersArr.length; i++) {
                const supplierProducts = await suppliersArr[i].getProducts({
                    where: {
                        type_id: parsedTypesId[0]
                    }
                });
                if (!supplierProducts[0]) {
                    suppliersArr[i].dataValues.price = Infinity;
                }
                else {
                    suppliersArr[i].dataValues.price = supplierProducts[0].dataValues.price;
                }
                const alreadyIn = samePrice.find(item => item === suppliersArr[i].dataValues.price);
                if (!alreadyIn) {
                    samePrice.push(suppliersArr[i].dataValues.price);
                }
            }
            samePrice.sort((a, b) => a - b);
            samePrice.forEach(async (price) => {
                const currentMatchSuppliers = await suppliersArr.filter(currentSupplier => currentSupplier.dataValues.price === price);
                preSortedSuppliers.push(currentMatchSuppliers);
            });
        };

        const runFilters = async () => {
            await getSupliersWhoHasSearchGoods();
            let res = '';
            for (let i = 0; i < filterParams.length; i++) {
                let secondArr = [];
                secondArr = [...preSortedSuppliers];
                preSortedSuppliers = [];
                if (filterParams[i] === 'supplier') {
                    if (res === '') {
                        res = getBestSuppliers(suppliers);
                    }
                    else {
                        const preRes = [];
                        for (let k = 0; k < secondArr.length; k++) {
                            preRes.push(await getBestSuppliers(secondArr[k]));
                        }
                        res = secondArr;
                    }
                }
                if (filterParams[i] === 'type') {
                    if (res === '') {
                        res = getMoreMatches(suppliers);
                    }
                    else {
                        const preRes = [];
                        for (let k = 0; k < secondArr.length; k++) {
                            preRes.push(await getMoreMatches(secondArr[k]));
                        }
                        res = secondArr;
                    }
                }
                if (filterParams[i] === 'price') {
                    if (res === '') {
                        res = getLowerPrices(suppliers);
                    }
                    else {
                        const preRes = [];
                        for (let k = 0; k < secondArr.length; k++) {
                            preRes.push(await getLowerPrices(secondArr[k]));
                        }
                        res = secondArr;
                    }
                }
                res = await res;
            }
            return res;
        };
        return await runFilters();
    };

    return product;
};
