const wrap = require('./wrap');
const express = require('express');
const sequelize = require('../dataBase');
const changeData = require('./changeData');

const router = express.Router();
const bucketGoods = sequelize.import('../models/bucketGoods');

const getAll = wrap(async (req, res) => {
    res.send(await bucketGoods.getAll());
});

const changeBucketData = wrap(changeData(bucketGoods));

router.get('/getBucketGoods', getAll);
router.post('/changeBucketGoods', changeBucketData);

module.exports = router;
