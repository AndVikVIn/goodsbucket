const changeData = data =>
    async (req, res) => {
        if (req.body.webix_operation === 'delete') {
            const result = await data.deleteOne(req.body.id);
            if (result === 1) {
                res.json({
                    code: 200,
                    message: 'Ok',
                    response: {msg: 'Recored deleted'}
                });
            }
            else {
                res.json({
                    code: 404,
                    message: 'Ok',
                    response: {msg: 'Not found'}
                });
            }
        }
        else if (req.body.webix_operation === 'update') {
            res.send(await data.updateOne(req.body.id, req.body));
        }
        else if (req.body.webix_operation === 'insert') {
            res.send(await data.createNew(req.body));
        }
    };

module.exports = changeData;
