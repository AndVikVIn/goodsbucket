const wrap = require('./wrap');
const express = require('express');
const sequelize = require('../dataBase');

const router = express.Router();
const product = sequelize.import('../models/products');

const returnSearchResults = wrap(async (req, res) => {
    res.send(await product.returnSearchResults(req.query.goods, req.query.searchParams));
});

router.get('/searchGoods', returnSearchResults);

module.exports = router;
