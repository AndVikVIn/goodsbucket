const wrap = require('./wrap');
const express = require('express');
const sequelize = require('../dataBase');

const router = express.Router();
const productType = sequelize.import('../models/productsType');

const getAll = wrap(async (req, res) => {
    res.send(await productType.getAll());
});

router.get('/getAllTypes', getAll);

module.exports = router;
