const express = require('express');

const router = express.Router();

router.use(require('./bucketGoods'));
router.use(require('./productType'));
router.use(require('./products'));

module.exports = router;
