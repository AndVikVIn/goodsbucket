const Sequelize = require('sequelize');

const sequelize = new Sequelize('productsDb', 'root', 'password', {
    dialect: 'mysql'
});

sequelize.sync();

module.exports = sequelize;
